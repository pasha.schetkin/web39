<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function increment(Request $request): \Illuminate\Http\RedirectResponse
    {
        $action = 1;
        if ($request->session()->exists('action')) {
            $action = $request->session()->get('action') + 1;
        }
        $request->session()->put('action', $action);
        return back()->with('success', 'Success increment!');
    }

    public function decrement(Request $request): \Illuminate\Http\RedirectResponse
    {
        if ($request->session()->exists('action')) {
            $action = $request->session()->get('action') - 1;
            $request->session()->put('action', $action);
            return back()->with('success', 'Success decrement!');
        } else {
            return back()->with('message', 'You dont click increment yet!');
        }
    }
}
