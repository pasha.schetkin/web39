<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/increment', [\App\Http\Controllers\ActionController::class, 'increment'])->name('action.increment');
Route::delete('/decrement', [\App\Http\Controllers\ActionController::class, 'decrement'])->name('action.decrement');

Auth::routes();
Route::get('/profile', [\App\Http\Controllers\ProfileController::class, 'index'])->name('users.profile');
